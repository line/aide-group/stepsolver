# stepsolver

A step by step variational solver mechanism

@aideAPI

## Presentation

This factory encapsulates step by step variational solver mechanism.

- The design choice here is to consider fully defined numerical value [Numeric](Numeric.html):
  - Bounded between a `min` and `max` value.
  - A numerical precision `eps` under which two values are indistinguishable.
  - A sampling `step` if the value lies in a non convex neighborhood.
  - A default and initial value.
  - A physical unit, if applicable.

- Here two basic algorithms are implemented:
  - An [Optimizer](Optimizer.html) minimizing a [Criterion](Criterion.html) allowing to locally find a solution to a system of equations in a deterministic way, without additional hyperparameters.
  - A [Controller](Controller.html) of a look-and-move [Driver](Driver.html) mechanism allowing to drive an external mechanism connected to the driver towards desired values.

- In both cases a step by step, slow but robust reproducible mechanism is obtained.


<a name='what'></a>

## Package repository

- Package files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/stepsolver'>https://gitlab.inria.fr/line/aide-group/stepsolver</a>
- Package documentation: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/stepsolver'>https://line.gitlabpages.inria.fr/aide-group/stepsolver</a>
- Source files: <a target='_blank' href='https://gitlab.inria.fr/line/aide-group/stepsolver/-/tree/master/src'>https://gitlab.inria.fr/line/aide-group/stepsolver/-/tree/master/src</a>
- Saved on <a target='_blank' href='https://archive.softwareheritage.org/browse/origin/directory/?origin_url=https://gitlab.inria.fr/line/aide-group/stepsolver'>softwareherirage.org</a>
- Version `1.1.1`
- License `CECILL-C`

## Installation

### User simple installation

- `npm install git+https://gitlab.inria.fr/line/aide-group/stepsolver.git`

### Co-developper installation

- See the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html#.install_as_developer'>related documentation</a>

Please refer to the <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild/install.html'>installation guide</a> for installation.

<a name='how'></a>

## Usage

### npm script usage
```
npm install --quiet : installs all package dependencies and sources.
npm run build: builds the different compiled, documentation and test files.
npm test     : runs functional and non-regression tests.
npm run clean: cleans installation files.
```

<a name='dep'></a>

## Dependencies

- <tt>aidesys: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidesys'>Basic system C/C++ interface routines to ease multi-language middleware integration</a></tt>
- <tt>wjson: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/wjson'>Implements a JavaScript JSON weak-syntax reader and writer</a></tt>

## devDependencies

- <tt>aidebuild: <a target='_blank' href='https://line.gitlabpages.inria.fr/aide-group/aidebuild'>Builds multi-language compilation packages and related documentation.</a></tt>

<a name='who'></a>

## Author

- Thierry Viéville <thierry.vieville@inria.fr>

