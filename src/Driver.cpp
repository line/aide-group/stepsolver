#include "Driver.hpp"
#include <cstddef>
#include "std.hpp"

namespace stepsolver {
  const double *Driver::get() const
  {
    aidesys::alert("illegal-state", "in stepsolver::Driver::get, the driver is undefined");
    return NULL;
  }
  void Driver::set(const double values[])
  {
    aidesys::alert("illegal-state", "in stepsolver::Driver::set, the driver is undefined");
  }
}
