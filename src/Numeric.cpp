#include "Numeric.hpp"
#include <cmath>
#include "std.hpp"

namespace stepsolver {
  Numeric::Numeric(String name, double precision, double min, double max, double zero, double step, String unit) : name(name), unit(unit), precision(precision), min(min), max(max), zero(std::isnan(zero) ? 0.5 * (max + min) : zero), step(step), count(std::isnan(precision) ? 0 : (unsigned int) ceil((max - min) / precision))
  {
    zero = std::isnan(zero) ? (min + max) / 2 : zero;
    precision = std::isnan(precision) || precision <= 0 ? NAN : precision;
    step = std::isnan(step) || step <= 0 ? NAN : step;
    aidesys::alert(!(min < max), "illegal-argument", "in stepsolver::Numeric::Numeric bad bounds we must have:  min=%e < max=%e", min, max);
    aidesys::alert(!(min < zero && zero < max), "illegal-argument", "in stepsolver::Numeric::Numeric bad zero value not in bounds must have: min=%e < zero=%e < max=%e", min, zero, max);
  }
  Numeric::Numeric(JSON parameter) :
    Numeric(parameter.get("name", ""),
            parameter.get("precision", NAN),
            parameter.get("min", -DBL_MAX),
            parameter.get("max", DBL_MAX),
            parameter.get("zero", NAN),
            parameter.get("step", NAN),
            parameter.get("unit", ""))
  {}
  Numeric::Numeric(String parameter) : Numeric(wjson::string2json(parameter, true))
  {}
  Numeric::Numeric(const char *parameter) : Numeric((String) parameter)
  {}
  Numeric::Numeric(const Numeric& numeric) :  name(numeric.name), unit(numeric.unit), precision(numeric.precision), min(numeric.min), max(numeric.max), zero(numeric.zero), step(numeric.step), count(numeric.count)
  {}

  std::string Numeric::asString() const
  {
    return aidesys::echo("{ name: '" + name + "' precision: %g min: %g max: %g zero: %g step: %g unit : '" + unit + "' }", precision, min, max, zero, step);
  }
  const std::vector < Numeric >& Numeric::json2numerics(JSON parameters_) {
    static std::vector < Numeric > parameters;
    parameters.clear();
    for(unsigned int i = 0; i < parameters_.length(); i++) {
      parameters.push_back(Numeric(parameters_.at(i)));
    }
    return parameters;
  }
}
