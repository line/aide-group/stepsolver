#include "Optimizer.hpp"
#include <cmath>
#include <set>

namespace stepsolver {
  Optimizer::Optimizer(const std::vector < Numeric > &parameters, Criterion& criterion) : parameters(parameters), criterion(criterion)
  {}
  Optimizer::Optimizer(JSON parameters, Criterion& criterion) : Optimizer(Numeric::json2numerics(parameters), criterion)
  {}
  Optimizer::Optimizer(String parameters, Criterion& criterion) : Optimizer(wjson::string2json(parameters, true), criterion)
  {}
  Optimizer::Optimizer(char const *parameters, Criterion& criterion) : Optimizer(wjson::string2json(parameters, true), criterion)
  {}
  double Optimizer::minimize(double values[], unsigned int loop) const
  {
    // Collects all initial values
    std::vector < std::vector < double >> ivalues;
    unsigned int count = 1;
    for(unsigned int i = 0; i < parameters.size(); i++) {
      ivalues.push_back({});
      if(std::isnan(parameters[i].step)) {
        ivalues[i].push_back(parameters[i].zero);
      } else {
        aidesys::alert(parameters[i].step <= 0, "illegal-argument", "in Optimizer::minimize parameters.step = %f <= 0 in « %s »", parameters[i].step, parameters[i].asString().c_str());
        for(double v = parameters[i].min + parameters[i].step; v <= parameters[i].max - parameters[i].step; v += parameters[i].step) {
          ivalues[i].push_back(v);
        }
        count *= ivalues[i].size();
      }
    }
    // Enumerates all initial values
    double values1[parameters.size()], c0 = 1e100;
    for(unsigned int c = 0; c < count; c++) {
      for(unsigned int i = 0, ic = c; i < parameters.size(); ic /= ivalues[i].size(), i++) {
        values1[i] = ivalues[i][ic % ivalues[i].size()];
      }
      double c1 = minimizeOnce(values1, loop);
      if(c == 0 || c1 < c0) {
        c0 = c1;
        for(unsigned int i = 0; i < parameters.size(); i++) {
          values[i] = values1[i];
        }
      }
    }
    return c0;
  }
  double Optimizer::minimizeOnce(double values[], unsigned int loop) const
  {
    // Axis of minimization
    struct Axis {
      // Value index
      unsigned int i, j;
      // Axis precision value
      double precision;
      // Last criterion decrement
      double dc;
      // Bounds
      double min, max;
    };
    // Axis sorting
    struct before {
      bool operator() (const Axis & lhs, const Axis & rhs) const {
        return lhs.dc == rhs.dc ? lhs.j < rhs.j : lhs.dc > rhs.dc;
      }
    };
    std::set < Axis, before > axes_sorted;
    // Axes initialisation
    double c0 = criterion.cost(values);
    for(unsigned int i = 0; i < parameters.size(); i++) {
      for(int d = 0; d < 2; d++) {
        Axis axis;
        axis.i = i, axis.j = d + 2 * i;
        if(!std::isnan(parameters[i].precision)) {
          axis.precision = d == 0 ? parameters[i].precision : -parameters[i].precision;
          axis.min = parameters[i].min, axis.max = parameters[i].max;
          // Estimates the initial decrease
          values[axis.i] += axis.precision;
          if(axis.min < values[axis.i] && values[axis.i] < axis.max) {
            double c1 = criterion.cost(values);
            axis.dc = c0 - c1;
            if(c1 < c0) {
              c0 = c1;
            } else {
              values[axis.i] -= axis.precision;
            }
          } else {
            axis.dc = 0;
            values[axis.i] -= axis.precision;
          }
          axes_sorted.insert(axis);
        }
      }
    }
    // Epsilon by precision iteration
    for(unsigned int l = 0; l < loop || loop == 0; l++) {
      std::set < Axis > ::iterator it = axes_sorted.begin();
      Axis axis = *it;
      axes_sorted.erase(it);
      if(axis.dc <= 0) {
        return c0;
      }
      values[axis.i] += axis.precision;
      if(axis.min < values[axis.i] && values[axis.i] < axis.max) {
        double c1 = criterion.cost(values);
        axis.dc = c0 - c1;
        if(c1 < c0) {
          c0 = c1;
        } else {
          values[axis.i] -= axis.precision;
        }
      } else {
        axis.dc = 0;
        values[axis.i] -= axis.precision;
      }
      axes_sorted.insert(axis);
    }
    return c0;
  }
}
