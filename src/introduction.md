@aideAPI

## Presentation

This factory encapsulates step by step variational solver mechanism.

- The design choice here is to consider fully defined numerical value [Numeric](Numeric.html):
  - Bounded between a `min` and `max` value.
  - A numerical precision `eps` under which two values are indistinguishable.
  - A sampling `step` if the value lies in a non convex neighborhood.
  - A default and initial value.
  - A physical unit, if applicable.

- Here two basic algorithms are implemented:
  - An [Optimizer](Optimizer.html) minimizing a [Criterion](Criterion.html) allowing to locally find a solution to a system of equations in a deterministic way, without additional hyperparameters.
  - A [Controller](Controller.html) of a look-and-move [Driver](Driver.html) mechanism allowing to drive an external mechanism connected to the driver towards desired values.

- In both cases a step by step, slow but robust reproducible mechanism is obtained.

