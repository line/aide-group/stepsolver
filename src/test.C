#include "Optimizer.hpp"
#include "Controller.hpp"
#include "std.hpp"

using namespace stepsolver;

int main()
{
  // Tests the default parameters
  {
    Numeric numeric("none", 1);
    // -printf("%s\n", numeric.asString().c_str());
  }
  // Tests the optimizer
  {
    std::vector < Numeric > parameters = {
      Numeric("x0", 0.1, -10, 10, 0, 5),
      Numeric("x1", 0.1, -10, 10, 0, 5),
      Numeric("x2", NAN, -10, 10)
    };
    class MyCriterion: public Criterion {
      double cost(const double values[])  const
      {
        double value = (values[0] < 0.5 || values[0] < 0.5) ?
                       3 : fabs(values[0] - 1.15) + fabs(values[1] - 2.15);
        // -printf("cost(x_opt: [%.3f %.3f %.3f]) = %g\n", values[0], values[1], values[2], value);
        return value;
      }
    }
    criterion;
    Optimizer optimizer(parameters, criterion);
    double values[3];
    optimizer.minimize(values);
    aidesys::alert(!(fabs(values[0] - 1.15) < parameters[0].precision && fabs(values[1] - 2.15) < parameters[1].precision), " illegal-state", "in stepsolver_test/optimizer, error = [%.g %.g %.g]", values[0] - 1, values[1] - 2, values[2] - 0);
  }
  // Tests the controller
  {
    std::string parameters = "[{name: x0 precision: 0.1 min: -10 max: 10} {name: x1 precision: 0.1 min: -10 max: 10}]";
    class MyDriver: public Driver {
      double values[2] = { 0, 0 };
      const double *get() const
      {
        // -printf("get [%g %g]\n", values[0], values[1]);
        return values;
      }
      void set(const double values[])
      {
        // -printf("set [%g %g]\n", values[0], values[1]);
        this->values[0] = values[0], this->values[1] = values[1];
      }
    }
    driver;
    double values[2] = { 1, 2 };
    Controller controller(parameters, driver);
    controller.iterate(values);
    aidesys::alert(!(fabs(values[0] - 1) < 0.1 && fabs(values[1] - 2) < 0.1), " illegal-state", "in stepsolver_test/controller, error = [%.g %.g]", values[0] - 1, values[1] - 2);
  }
  return 0;
}
