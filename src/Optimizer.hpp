#ifndef __stepsolver_Optimizer__
#define __stepsolver_Optimizer__

#include "Numeric.hpp"
#include "Criterion.hpp"

namespace stepsolver {
  /**
   * @class Optimizer
   * @description Implements a criterion function minimizer.
   * @param {Array<Numeric>} parameters A ``std::vector<Numeric>` with the solver parameters.
   * @param {Criterion} criterion The criterion function to minimize.
   */
  class Optimizer {
private:
    const std::vector < Numeric > parameters;
    Criterion& criterion;
public:
    Optimizer(const std::vector < Numeric > &parameters, Criterion & criterion);
    Optimizer(JSON parameters, Criterion & criterion);
    Optimizer(String parameters, Criterion & criterion);
    Optimizer(const char *parameters, Criterion & criterion);

    /**
     * @function minimize
     * @memberof Optimizer
     * @instance
     * @description Incrementally locally minimizes the criterion with respect to the given parameter values.
     *  - If the `step` is undefined, the optimization starts with the `zero` default values.
     *  - If the `step` is defined, the optimizations start at regular samples in the `[min, max]` intervals.
     * @param {Array<double>} values The optimal parameter values on output.
     * - The `values` array size _must_ equal the `parameters` array size, otherwise an undetectable segmentation fault occurs.
     * @param {unit} [loop = 0] Maximal numer of iteration, 0 means unbounded.
     * @return {double} The final optimized criterion value.
     */
    double minimize(double values[], unsigned int loop = 0) const;
private:
    double minimizeOnce(double values[], unsigned int loop = 0) const;
  };
}
#endif
