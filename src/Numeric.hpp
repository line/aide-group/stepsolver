#ifndef __stepsolver_Numeric__
#define __stepsolver_Numeric__

#include <math.h>
#include "Value.hpp"
#include <float.h>

namespace stepsolver {
  /**
   * @class Numeric
   * @description Specifies a bounded finite precision numerical value.
   * - The numeric parameter values can also be given as a JSON structure, or a String.
   * @param {string} [name] The numerical value name.
   * @param {double} [precision=NAN] The positive numerical precision.
   *  - This is the value under which two values are indistinguishable.
   *  - The default means `NAN` that the value is a constant, not to be adjusted.
   * @param {double} [min=-DBL_MAX] The numerical minimal value.
   * @param {double} [max=DBL_MAX] The numerical maximal value.
   * @param {double} [zero=NAN] The numerical default and initial value.
   *  - The default is `(max + min)/2`.
   * @param {double} [step=NAN] The positive sampling step, in order to sample the [min, max] interval.
   *  - The default corresponds to a value that is not be sampled, often corresponding to a convex neighborhood.
   * @param {string} [unit=""] The numerical unit.
   */
  struct Numeric {
public:
    const std::string name, unit;
    const double precision, min, max, zero, step;
    const unsigned int count;

    /**
     * @member {uint} count
     * @memberof Numeric
     * @instance
     * @description The number of defined values between min and max, given the step.
     */
    Numeric(String name, double precision, double min = -DBL_MAX, double max = DBL_MAX, double zero = NAN, double step = NAN, String unit = "");
    Numeric(JSON parameter);
    Numeric(String parameter);
    Numeric(const char *parameter);
    Numeric(const Numeric& numeric);

    /**
     * @function asString
     * @memberof Numeric
     * @instance
     * @description Returns the numeric parameters as a wJSON string.
     * @return {string} The string view of the parameters.
     */
    std::string asString() const;
public:
    static const std::vector < stepsolver::Numeric > &json2numerics(JSON parameters_);
  };
}

#endif
