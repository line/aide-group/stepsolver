#include "Controller.hpp"

namespace stepsolver {
  Controller::Controller(const std::vector < Numeric > &parameters, Driver& driver) : parameters(parameters), driver(driver)
  {}
  Controller::Controller(JSON parameters, Driver& driver) : Controller(Numeric::json2numerics(parameters), driver)
  {}
  Controller::Controller(String parameters, Driver& driver) : Controller(wjson::string2json(parameters, true), driver)
  {}
  bool Controller::iterate(double values[], unsigned int loop) const
  {
#define VERBOSE_iterate_NO
    double values1[parameters.size()];
    for(unsigned int l = 0; l < loop || loop == 0; l++) {
      const double *values0 = driver.get();
      bool done = true;
      for(unsigned int i = 0; i < parameters.size(); i++) {
        values1[i] = 0.5 * (values0[i] + values[i]),
        done &= fabs(values0[i] - values[i]) < parameters[i].precision;
      }
#ifdef VERBOSE_iterate
      {
        printf("{ t: %d \n\t   values0: [ ", l);
        for(unsigned int i = 0; i < parameters.size(); i++) {
          printf("%6.2g ", values0[i]);
        }
        printf("]\n\t   values1: [ ");
        for(unsigned int i = 0; i < parameters.size(); i++) {
          printf("%6.2g ", values1[i]);
        }
        printf("]\n\terror/precision: [ ");
        for(unsigned int i = 0; i < parameters.size(); i++) {
          printf("%6.2g ", fabs(values0[i] - values[i]) / parameters[i].precision);
        }
        printf("]\n");
      }
#endif
      if(done || l == loop - 1) {
        for(unsigned int i = 0; i < parameters.size(); i++) {
          values[i] = values0[i];
        }
        return done;
      } else {
        driver.set(values1);
      }
    }
    return false;
  }
}
