#ifndef __stepsolver_Controller__
#define __stepsolver_Controller__

#include "Numeric.hpp"
#include "Driver.hpp"

namespace stepsolver {
  /**
   * @class Controller
   * @description Implements an iterative driver mechanism.
   * @param {Array<Numeric>} parameters A ``std::vector<Numeric>` with the solver parameters.
   * @param {Driver} driver The driver function to iterate on.
   */
  class Controller {
private:
    const std::vector < Numeric > parameters;
    Driver& driver;
public:
    Controller(const std::vector < Numeric > &parameters, Driver & driver);
    Controller(JSON parameters, Driver & driver);
    Controller(String parameters, Driver & driver);

    /**
     * @function iterate
     * @memberof Controller
     * @instance
     * @description Incrementally locally iterates the driver with respect to the given parameter values.
     * @param {Array<double>} values The desired parameter values.
     *  - On input: the a-priori desired parameter values.
     *  - On ouput: the a-posteri obtained parameter values.
     * @param {unit} [loop = 0] Maximal number of iteration, 0 means unbounded.
     * @return {bool} True if the iteration has converged, false if it failed.
     */
    bool iterate(double values[], unsigned int loop = 0) const;
  };
};

#endif
