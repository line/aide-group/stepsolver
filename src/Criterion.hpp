#ifndef __stepsolver_Criterion__
#define __stepsolver_Criterion__

namespace stepsolver {
  /**
   * @class Criterion
   * @description Specifies a criterion function to minimize.
   */
  class Criterion {
public:
    virtual ~Criterion() {}

    /**
     * @function cost
     * @memberof Criterion
     * @instance
     * @abstract
     * @description Returns a positive value, defining the variational task, i.e. the criterion or cost to minimize.
     * @param {Array<double>} values The parameter values.
     * @return {double} The corresponding criterion value.
     */
    virtual double cost(const double values[]) const;
  };
}
#endif
