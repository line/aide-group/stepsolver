#include "Criterion.hpp"
#include <math.h>
#include "std.hpp"

namespace stepsolver {
  double Criterion::cost(const double values[]) const
  {
    aidesys::alert("illegal-state", "in stepsolver::Criterion::cost, the criterion cost is undefined");
    return NAN;
  }
}
