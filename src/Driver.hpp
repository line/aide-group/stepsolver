#ifndef __stepsolver_Driver__
#define __stepsolver_Driver__

namespace stepsolver {
  /**
   * @class Driver
   * @description Specifies a look-and-move driver mechanism to control.
   */
  class Driver {
public:
    virtual ~Driver() {}

    /**
     * @function get
     * @memberof Driver
     * @instance
     * @abstract
     * @description Inputs the observed values.
     * @return {Array<double>} The observed parameter values.
     */
    virtual const double *get() const;

    /**
     * @function set
     * @memberof Driver
     * @instance
     * @abstract
     * @description Outputs the desired values.
     * @param {Array<double>} values The desired parameter values.
     */
    virtual void set(const double values[]);
  };
}

#endif
